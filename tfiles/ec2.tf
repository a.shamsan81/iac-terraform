# Get the latest ami in the region owned by Amazon

data "aws_ami" "amzlinux" {
    most_recent = true
    owners      = ["amazon"]
    filter {
        name    = "name"
        values  = ["amzn2-ami-hvm-*-gp2"]
    }
    
    filter {
        name    = "root-device-type"
        values  = ["ebs"] 
    }

    filter {
        name    = "virtualization-type"
        values  = ["hvm"] 
    }
}

# Create EC2 Resource
resource "aws_instance" "myec2" {
  ami           = data.aws_ami.amzlinux.id
  instance_type = var.tfvarType
  tags = {
      Name = var.ec2Name
  }
}

output "ec2-details" {
  value         = aws_instance.myec2
}
