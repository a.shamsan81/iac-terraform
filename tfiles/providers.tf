terraform {
  required_providers {
    aws = {
      source    = "hashicorp/aws"
      version   = "~> 4.0"
    }
  }
}

# Configure the Provider
provider "aws" {
  region        		= var.tfvarRegion
}
